package ael.education.controller;

import ael.education.entity.Book;
import ael.education.service.BookService;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;
import org.jboss.logging.Logger;

@Path("/books")
public class BookEnpoints {

    private static final Logger log = Logger.getLogger(BookEnpoints.class); 
    
    @Inject
    BookService bookservice;

    /**
     * Получение полного списка книг
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Book> getAllBooks() {
        List<Book> books = bookservice.getAllBooks();
        return books;
    }

    /**
     * Добавление книги
     *
     * @param book
     * @return
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Book createBook(Book book) {
        log.info("Поступил запрос на добавление книги");
        Book savedBook = bookservice.saveBook(book);        
        return savedBook;
    }

    /**
     * Удаление книги
     *
     * @param book
     * @return
     */
    
    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteBookById(@PathParam("id") UUID id) {
        log.info("Поступил запрос на удаление книги id = "+id);
    }
    
    
    /**
     * Поиск книги 
     * 
     * 
     * @param id
     * @return 
     */
    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Book getBookById(@PathParam("id") UUID id) {

        log.info("Поступил запрос на поиск книги id = "+id);
        
        Book book = bookservice.getBookById(id);
        return book;
    }

    
    
}
