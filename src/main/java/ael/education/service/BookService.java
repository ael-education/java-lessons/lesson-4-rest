/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ael.education.service;

import ael.education.entity.Book;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author developer
 */

// Все сервисы должы быть глобально доступны в пределах приложения с использованием 
// аннотоции @ApplicationScoped
@ApplicationScoped
public class BookService {
   
    
    // Менеджер для работы с сущностями
    @Inject
    EntityManager em;

    // JPQL
    //  students = (List<Student>) em.createQuery("Select t from " + Student.class.getSimpleName() + " t").getResultList();
    //
    
   /*
    if (id != null) {
            container.addInfos("Получение студента с UUID= " + id + " ...");
            try {
                student = em.find(Student.class, id);
            } catch (Exception e) {
                container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении студента : " + e.getMessage());
            }
        } else {
            container.addError("Ошибка получения студента: входной экземпляр UUID пустой");
        }

    */
    
    @Transactional
    public void delete(Book book) {
        em.remove(book);
    }

    @Transactional
    public List<Book> getAllBooks() {
        List<Book> books = em.createQuery("Select b from " + Book.class.getSimpleName() + " b").getResultList();
        return books;
    }

    
    /**
     * Обновление экземпляра в базе данных 
     * 
     * @param book
     * @return 
     */
    @Transactional
    public Book updateBook(Book book) {
        em.merge(book); // merge  - слияние полей из экземпляра book c полями экземпляра, хранящегося в базе данных
        return book;
    }

    /**
     * Создание записи о книге в базе данных
     *
     * @param book  экземпляр книги
     * @return
     */
    @Transactional
    public Book saveBook(Book book) {        
        em.persist(book);   // Сохранение в памяти и постановка в очередь для сохранения в файле БД
        em.flush();         // Сброс содержимого памяти на диск
        return book;
    }
    
    @Transactional
    public Book getBookById(UUID id) {
        Book book = em.find(Book.class, id);

        return book;
    }
    
}
