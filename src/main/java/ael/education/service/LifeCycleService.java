/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ael.education.service;



import ael.education.entity.Book;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import java.time.LocalDate;
import java.time.Month;
import org.jboss.logging.Logger;
/**
 * Сервис жизненного цикла: 
 * Запускает методы (onStart) при старте и остановке сервер ()
 * 
 * @author developer
 */


@ApplicationScoped
public class LifeCycleService {

    private static final Logger log = Logger.getLogger(LifeCycleService.class); 
    
    
    // экземпляр BookService внедрен
    @Inject
    BookService bookService;
    
    
    /**
     * Автоматический запуск при старте приложения
     *
     * @Observes - аннотация слушателя событий жизненного цикла
     *
     * @param event
     */
    public void onStart(@Observes StartupEvent event) {
        log.info("Старт сервера...");

        // Создание 2-х книг и запись в базу данных
        Book book1 = new Book();
        book1.setTitle("Война миров");
        book1.setAuthor("Иванов А.П");
        book1.setPublicationDate(LocalDate.of(1902, Month.MARCH, 1));

        Book book2 = new Book();
        book2.setTitle("Война миров. Часть 2");
        book2.setAuthor("Иванов А.П");
        book2.setPublicationDate(LocalDate.of(1903, Month.MARCH, 1));
        
        // первоначальное сохранение - обращение к методу saveBook класса BookService
        bookService.saveBook(book1);   // Сохранение 1 книги 
        bookService.saveBook(book2);   // Сохранение 2 книги  

        // изменение поля
        book1.setTitle("Война миров и ежиков");
        // обновление книги в базе данных 
        bookService.updateBook(book1);
        

        log.info("Сервер стартовал");
    }

    public void onStop(@Observes ShutdownEvent event) {
        
        log.info("Остановка  сервера...");
        
        log.info("Сервер остановлен");

    }
    
}
